#include <iostream>
#include "opOverClass.h"
using namespace std;

int main()
{
    OpOverClass u(23, 45); 	//Line 1
	OpOverClass v(12,10);		//Line 2
				//Line 4

	cout<<"Line 5: u = ";		//Line 5
	u.print();			//Line 6; output u
	cout<<endl;			//Line 7

	cout<<"Line 8: v = ";		//Line 8
	v.print();			//Line 9; output v
	cout<<endl;			//Line 10

		u++;
		++v;	//Line 11; add u and v

	cout<<"Line 12: w1 = ";	//Line 12
	v.print();			//Line 13; output w1
	cout<<endl;			//Line 14
}
